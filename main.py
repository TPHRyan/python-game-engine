import test_classes

from rpgengine import director
from rpgengine import init

if __name__ == '__main__':
    game_window = init()
    main_scene = test_classes.HelloScene()
    director.run(main_scene)
