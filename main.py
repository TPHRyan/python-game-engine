import test_classes

from tphpe.controller import EntityController
from tphpe.controller import ObjectController
from tphpe.game import Game

if __name__ == '__main__':
    game = Game()
    main_scene = test_classes.HelloScene()
    main_scene.add_controller(
        key_binds=test_classes.test_entity_buttons,
        cls_controller=EntityController,
        ents=main_scene.entity_manager.entities,
        active_entity=1
    )
    main_scene.add_controller(
        key_binds=test_classes.test_object_buttons,
        cls_controller=ObjectController,
        obj=main_scene
    )
    main_scene.add_controller(
        key_binds=test_classes.test_signal_buttons,
        cls_controller=ObjectController,
        obj=main_scene
    )
    game.run(main_scene)
