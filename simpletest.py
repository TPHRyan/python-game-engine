import rpgengine.menu as menu
from rpgengine.menu import MenuCommand
from rpgengine.menu import MenuItem


def go_back(calling_menu):
    calling_menu.back()

submenu1 = menu.MenuData(
    MenuItem('Option 1-1', MenuCommand(print, 'Option 1-1')),
    MenuItem('Option 1-2', MenuCommand(print, 'Option 1-2')),
    MenuItem('Go Back', MenuCommand(go_back, uses_menu=True))
)
submenu2 = menu.MenuData(
    MenuItem('Option 2-1', MenuCommand(print, 'Option 2-1'))
)

data1 = menu.MenuData(
    MenuItem('Submenu 1', submenu1),
    MenuItem('Submenu 2', submenu2)
)
stack1 = menu.MenuStack(data1)
res1 = stack1.activate()
res2 = stack1.activate()
stack1.next()
res3 = stack1.activate()
stack1.next()
res4 = stack1.activate()
stack1.next()
res5 = stack1.activate()
res6 = stack1.activate()

