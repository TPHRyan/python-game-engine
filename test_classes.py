import pyglet.window.key as pyglet_key
from cocos.collision_model import AARectShape
from cocos.sprite import Sprite

import tphpe.menu.command as command
import tphpe.menu.signal as menu_signals
from tphpe.base import Axis
from tphpe.base import make_vector2
from tphpe.command import MoveCommand
from tphpe.command import ObjectMethodCommand
from tphpe.menu import MenuCommand
from tphpe.menu import MenuData
from tphpe.menu import MenuItem
from tphpe.scene import TiledGameScene


test_entity_buttons = {
    pyglet_key.LEFT: (
        MoveCommand(axis=Axis.X, velocity=(-40, 0)),
        MoveCommand(axis=Axis.X, velocity=(0, 0))),
    pyglet_key.RIGHT: (
        MoveCommand(axis=Axis.X, velocity=(40, 0)),
        MoveCommand(axis=Axis.X, velocity=(0, 0))),
    pyglet_key.UP: (
        MoveCommand(axis=Axis.Y, velocity=(0, 40)),
        MoveCommand(axis=Axis.Y, velocity=(0, 0))),
    pyglet_key.DOWN: (
        MoveCommand(axis=Axis.Y, velocity=(0, -40)),
        MoveCommand(axis=Axis.Y, velocity=(0, 0)))
}

test_object_buttons = {
    pyglet_key.ESCAPE: (ObjectMethodCommand(method_name='open_menu'),)
}

test_signal_buttons = {
    pyglet_key.ENTER: (ObjectMethodCommand(
            method_name='signal_layer', method_args=(menu_signals.MENU_ACTIVATE,))),
    pyglet_key.SPACE: (ObjectMethodCommand(
            method_name='signal_layer', method_args=(menu_signals.MENU_ACTIVATE,))),
    pyglet_key.ESCAPE: (ObjectMethodCommand(
            method_name='signal_layer', method_args=(menu_signals.MENU_CLOSE,))),
    pyglet_key.UP: (ObjectMethodCommand(
            method_name='signal_layer', method_args=(menu_signals.MENU_UP,))),
    pyglet_key.DOWN: (ObjectMethodCommand(
            method_name='signal_layer', method_args=(menu_signals.MENU_DOWN,)))
}


class HelloScene(TiledGameScene):
    def __init__(self, *args, **kwargs):
        hello_menu_data = MenuData(
            MenuItem('Option1', MenuCommand(print, 'hi! 1')),
            MenuItem('Item2', MenuCommand(print, 'hello! 2')),
            MenuItem('Close', command.close_menu),
            MenuItem('Quit', MenuCommand(self.end))
        )
        super().__init__(*args, tilemap='assets/maps/testmap1.tmx',
                         ground_layers=['ground', 'buildings'],
                         menu_data=hello_menu_data, **kwargs)
        self.system_manager.enable_system(
            'cocosnode',
            'motion',
            'collision'
        )
        self.load_music('assets/severe_tire_damage.mp3')

        self.spawn_entity({
            'cocosnode': {
                'node': Sprite('assets/orb.png', position=(80, 80))
            },
            'motion': {
                'velocity': (0, 0)
            },
            'collision': {
                'cshape': AARectShape(make_vector2((80, 80)), 15, 15)
            }
        })
        self.spawn_entity({
            'cocosnode': {
                'node': Sprite('assets/orb.png', position=(160, 160))
            },
            'collision': {
                'cshape': AARectShape(make_vector2((160, 160)), 15, 15)
            }
        })

    def update(self, dt):
        super().update(dt)

    def on_enter(self):
        super().on_enter()
        self.resume_game()

    def on_exit(self):
        super().on_exit()
        self.pause_game()
