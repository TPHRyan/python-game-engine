import cocos.actions as actions
import pyglet.window.key as pyglet_key

from cocos.director import director
from cocos.sprite import Sprite
from rpgengine.scene import TiledGameScene


class HelloScene(TiledGameScene):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, tilemap='assets/maps/testmap1.tmx',
                         ground_layers=['ground', 'buildings'], **kwargs)
        self.load_music('assets/severe_tire_damage.mp3')

        self.orb = Sprite('assets/orb.png', position=(80, 80))
        self.add_sprite(self.orb)

    def update(self, dt):
        super().update(dt)
        self.scroller.force_focus(*self.orb.position)

    def on_key_press(self, key, modifiers):
        if key == pyglet_key.W:
            self.move_orb((0, 32))
        elif key == pyglet_key.A:
            self.move_orb((-32, 0))
        elif key == pyglet_key.S:
            self.move_orb((0, -32))
        elif key == pyglet_key.D:
            self.move_orb((32, 0))
        return super().on_key_press(key, modifiers)

    def move_orb(self, moveby):
        self.orb.do(actions.MoveBy(moveby, duration=0.25))

    def on_enter(self):
        super().on_enter()
        self.resume_game()

    def on_exit(self):
        super().on_exit()
        self.pause_game()
