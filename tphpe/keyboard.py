import pyglet.event
from collections.abc import MutableMapping
from collections import defaultdict
from tphpe.base import Root
from tphpe.event import EventDispatcher


class Keyboard(Root, EventDispatcher, MutableMapping):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.mappings = defaultdict(lambda: False)

    def __getitem__(self, item):
        return self.mappings[item]

    def __setitem__(self, key, value):
        self.mappings[key] = value

    def __delitem__(self, key):
        del self.mappings[key]

    def __len__(self):
        return len(self.mappings)

    def __iter__(self):
        yield from self.mappings.keys()

    def on_key_press(self, key, modifiers):
        self[key] = True
        self.dispatch_event('input_press', key, modifiers)
        return pyglet.event.EVENT_HANDLED

    def on_key_release(self, key, modifiers):
        self[key] = False
        self.dispatch_event('input_release', key, modifiers)
        return pyglet.event.EVENT_HANDLED

Keyboard.register_event_type('input_press')
Keyboard.register_event_type('input_release')
