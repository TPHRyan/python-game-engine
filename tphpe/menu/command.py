from tphpe.menu.structure import MenuCommand


def _close_menu_command(ptr):
    ptr.close()

close_menu = MenuCommand(_close_menu_command, uses_menu=True)
