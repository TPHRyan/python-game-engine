import pyglet.event

from tphpe.base import Root
from tphpe.menu.exception import MenuException
from tphpe.menu.signal import MENU_CLOSE, MENU_UP, MENU_DOWN, MENU_ACTIVATE
from tphpe.menu.structure import MenuCommand
from tphpe.menu.structure import MenuData
from tphpe.menu.view import ListView
from tphpe.menu.view import MenuLayer


class Menu(Root):
    def __init__(self, scene, *args, menu_data=MenuData(),
                 menu_view=ListView, **kwargs):
        super().__init__(*args, **kwargs)
        self.data = menu_data
        self.view = MenuLayer(view_type=menu_view, key_handler=self)
        self.stack = MenuStack(self, menu_data=menu_data)
        self.scene = scene

    def open(self):
        self.scene.pause_game()
        self.view.build(self.data)
        self.scene.add_layer(self.view)

    def close(self):
        self.scene.pop_layer()
        self.scene.resume_game()

    def on_signal(self, signal):
        if signal == MENU_CLOSE:
            self.close()
        elif signal == MENU_UP:
            self.stack.previous()
            self.view.sync(self.stack.position_vector)
        elif signal == MENU_DOWN:
            self.stack.next()
            self.view.sync(self.stack.position_vector)
        elif signal == MENU_ACTIVATE:
            self.stack.activate()
        else:
            return pyglet.event.EVENT_UNHANDLED
        return pyglet.event.EVENT_HANDLED


class MenuPointer(Root):
    """
    Represents the user's current position in a submenu
    """
    def __init__(self, stack, *args, menu_data=None, **kwargs):
        super().__init__(*args, **kwargs)
        if menu_data is None:
            raise MenuException('Menu pointer created with no data!')
        self._menu_data = menu_data
        self._pos = menu_data.default_position
        self._stack = stack

    @property
    def position(self):
        return self._pos

    def reset(self):
        self._pos = 0

    def back(self):
        if len(self._stack) >= 1:
            self._stack.pop()

    def close(self):
        self._stack.close()

    def previous(self):
        if self._pos > 0:
            self._pos -= 1

    def next(self):
        if self._pos < len(self._menu_data) - 1:
            self._pos += 1

    def activate(self):
        pointed_item = self._menu_data[self._pos]
        if isinstance(pointed_item.action, MenuData):
            return MenuPointer(self._stack, menu_data=pointed_item.action)
        elif isinstance(pointed_item.action, MenuCommand):
            return pointed_item.action(self)

    def __iter__(self):
        yield from self._menu_data


class MenuStack(Root):
    """
    Represents the user's current position in series of menus
    """
    def __init__(self, menu, menu_data=None, **kwargs):
        super().__init__(**kwargs)
        if menu_data is None:
            menu_data = MenuData()
        self.menu = menu
        self._stack = []
        self.open(menu_data)

    def __getattr__(self, item):
        return getattr(self._stack[-1], item)

    def __len__(self):
        return len(self._stack)

    def clear(self):
        self._stack.clear()

    def pop(self):
        if len(self._stack) == 1:
            self.menu.close()
            return None
        return self._stack.pop()

    def open(self, menu_data):
        self.clear()
        self._stack.append(MenuPointer(self, menu_data=menu_data))

    def close(self):
        self.menu.close()

    @property
    def position_vector(self):
        pos_v = []
        for ptr in self._stack:
            pos_v.append(ptr.position)
        return tuple(pos_v)

    def activate(self):
        res = self._stack[-1].activate()
        if isinstance(res, MenuPointer):
            self._stack.append(res)
            return None
        return res

    def is_empty(self):
        return len(self._stack) == 0
