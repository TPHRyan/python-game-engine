from cocos.actions import FadeIn, FadeOut
from cocos.director import director
from cocos.layer import ColorLayer
from cocos.layer import MultiplexLayer
from cocos.text import Label
from collections import ChainMap
from pyglet.event import EVENT_UNHANDLED
from tphpe.menu.menu import MenuData


class ListView(ColorLayer):
    """
    Represents a menu in the form of a list.
    """
    defaults = {
        'font_size': 32,
        'font_name': 'Courier New',
        'activate_bold': True,
        'padding': 24,
        'fade_time': 0.05,
    }

    def __init__(self, *args, **kwargs):
        super().__init__(0, 0, 0, 128, **kwargs)
        self.config = ChainMap(kwargs, ListView.defaults)
        self.menu_labels = {}
        self.selected = 0

    def build(self, menu_data):
        for c in self.get_children():
            self.remove(c)
        screen_x, screen_y = director.get_window_size()
        font_size = self.config['font_size']
        padding = self.config['padding']
        menu_height = len(menu_data) * (font_size + padding)
        current_y = screen_y // 2 + menu_height // 2
        for i, d in enumerate(menu_data):
            inactive_label = self.label_from_item(d, (screen_x // 2, current_y))
            active_label = self.label_from_item(d, (screen_x // 2, current_y),
                                                selected=True)
            inactive_label.opacity = 0 if i == self.selected else 255
            active_label.opacity = 255 if i == self.selected else 0
            self.add(inactive_label)
            self.add(active_label)
            self.menu_labels[i] = (inactive_label, active_label)
            current_y -= font_size + padding
        return self

    def label_from_item(self, item, pos, selected=False):
        is_bold = self.config['activate_bold'] if selected else False
        return Label(item.text, position=pos,
                     anchor_x='center', anchor_y='top',
                     font_size=self.config['font_size'],
                     font_name=self.config['font_name'],
                     bold=is_bold)

    def select(self, ind):
        if self.selected != ind:
            ft = self.config['fade_time']
            self.menu_labels[self.selected][0].do(FadeIn(ft))
            self.menu_labels[self.selected][1].do(FadeOut(ft))
            self.menu_labels[ind][0].do(FadeOut(ft))
            self.menu_labels[ind][1].do(FadeIn(ft))
            self.selected = ind
        return self


class MenuLayer(MultiplexLayer):
    """
    Represents a set of menus as they will be displayed in a big-picture form
    """
    def __init__(self, *args, view_type=ListView, key_handler=None, **kwargs):
        # Create a view for this menu in the constructor
        super().__init__(view_type(), *args)
        self.menu_index = {}
        self.view_type = view_type
        self.key_handler = key_handler

    def build(self, menu_data):
        children = self.get_children()
        # Get a clean slate
        for c in children[1:]:
            self.remove(c)
        # Build the layer we created in the constructor
        children[0].build(menu_data)

        # Create layers for any submenus and build them
        for i, menu_item in enumerate(menu_data):
            if isinstance(menu_item.action, MenuData):
                self.menu_index[i] = len(self.children)
                self.add(
                    MenuLayer(view_type=self.view_type).build(menu_item.action)
                )
        return self

    def sync(self, pos_vector):
        # If we have 1 position left then it's the selected item in the menu
        if len(pos_vector) == 1:
            self.switch_to(0)
            self.children[0][1].select(pos_vector[0])
        # Otherwise we switch to the correct submenu and let it deal with
        # the rest.
        else:
            child_index = self.menu_index[pos_vector[0]]
            self.switch_to(child_index)
            self.children[0][1].select(pos_vector[0])
            self.children[child_index][1].sync(pos_vector[1:])
        return self

    def set_key_handler(self, kh):
        self.key_handler = kh

    def on_signal(self, signal):
        if self.key_handler is not None:
            return self.key_handler.on_signal(signal)
        return EVENT_UNHANDLED
