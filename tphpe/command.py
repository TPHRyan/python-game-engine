from abc import ABCMeta
from abc import abstractmethod

from pyglet.event import EVENT_HANDLED
from pyglet.event import EVENT_UNHANDLED

from tphpe.base import Axis
from tphpe.base import Root


class Command(Root, metaclass=ABCMeta):
    """
    Commands: Abstracted functionality for use in menus etc.
    """
    @abstractmethod
    def __call__(self, *args, **kwargs):
        return super().__call__(*args, **kwargs)


class ObjectCommand(Command):
    """
    ObjectCommand: Calls arbitrary methods on an object
    """
    @abstractmethod
    def __call__(self, obj, *args, **kwargs):
        return super().__call__(obj, *args, **kwargs)


class ObjectMethodCommand(ObjectCommand):
    """
    ObjectMethodCommand: Calls arbitrary method on its object
    """
    def __init__(self, *args, method_name='do_nothing',
                 method_args=(), **kwargs):
        super().__init__(*args, **kwargs)
        self.method_name = method_name
        self.method_args = method_args

    def __call__(self, obj, *args, **kwargs):
        if hasattr(obj, self.method_name):
            return getattr(obj, self.method_name)(*self.method_args)


class EntityCommand(Command, metaclass=ABCMeta):
    """
    ObjectCommand: Abstracted functionality
    for use on ingame entities
    """
    @abstractmethod
    def __call__(self, ent, *args, **kwargs):
        return super().__call__(ent, *args, **kwargs)


class MoveCommand(Command):
    def __init__(self, *args, axis=Axis.BOTH, velocity=(0, 0), **kwargs):
        super().__init__(*args, **kwargs)
        self.axis = axis
        self.velocity = velocity

    def __call__(self, ent, *args, **kwargs):
        if 'motion' in ent:
            if self.axis == Axis.BOTH:
                ent['motion']['velocity'] = self.velocity
            elif self.axis == Axis.X:
                ent['motion']['velocity'] = (
                    self.velocity[Axis.X.value],
                    ent['motion']['velocity'][Axis.Y.value]
                )
            elif self.axis == Axis.Y:
                ent['motion']['velocity'] = (
                    ent['motion']['velocity'][Axis.X.value],
                    self.velocity[Axis.Y.value]
                )
            return EVENT_HANDLED
        return EVENT_UNHANDLED
