import cocos.tiles
import pyglet.media as media
from pyglet.event import EVENT_HANDLED
from pyglet.event import EVENT_UNHANDLED

from cocos.director import director
from cocos.layer.scrolling import ScrollableLayer
from cocos.layer.scrolling import ScrollingManager
from cocos.scene import Scene
from pyglet.event import EventDispatcher
from tphpe.entities import EntityManager
from tphpe.controller import Controller
from tphpe.layer import GameLayer
from tphpe.menu import ListView
from tphpe.menu import Menu
from tphpe.menu import MenuData
from tphpe.menu import MenuLayer
from tphpe.system import SystemManager


class GameScene(Scene, EventDispatcher):
    def __init__(self, *args, internal_layer=GameLayer,
                 menu_data=MenuData(), menu_view_type=ListView, **kwargs):
        super().__init__(*args)
        self.layers = []

        self._game_layer = internal_layer()
        self.add_layer(self._game_layer)

        self.entity_manager = EntityManager()
        self.system_manager = SystemManager(game_scene=self)
        self.entity_manager.push_handlers(self.system_manager.on_entity_spawn)
        self.entity_manager.push_handlers(self.system_manager.on_entity_death)

        self._player = media.Player()
        self.controllers = set()
        self.music = None
        self.menu = Menu(self, menu_data=menu_data, menu_view=menu_view_type)

    def update(self, dt):
        self.entity_manager.update(dt)
        self.system_manager.update(dt)

    def pause_game(self):
        self.pause_music()
        self.entity_manager.pause()
        self.system_manager.pause()
        self._game_layer.pause()

    def resume_game(self):
        self.play_music()
        self.entity_manager.resume()
        self.system_manager.resume()
        self._game_layer.resume()

    def add_layer(self, layer):
        self.layers.append(layer)
        self.add(layer)

    def pop_layer(self):
        self.remove(self.layers.pop())

    def signal_layer(self, signal):
        layer = self.layers[-1]
        if hasattr(layer, 'on_signal'):
            return layer.on_signal(signal)
        return EVENT_UNHANDLED

    def add_to_game(self, *args, **kwargs):
        self._game_layer.add(*args, **kwargs)

    def remove_from_game(self, *args, **kwargs):
        self._game_layer.remove(*args, **kwargs)

    def spawn_entity(self, entity, **kwargs):
        self.entity_manager.spawn(entity, **kwargs)

    def kill_entity(self, entity_id):
        self.entity_manager.kill(entity_id)

    def add_controller(self, key_binds=None, *,
                       cls_controller=Controller, **kwargs):
        self.controllers.add(cls_controller(btns=key_binds, **kwargs))

    def remove_controller(self, controller):
        if controller in self.controllers:
            self.controllers.remove(controller)

    def open_menu(self):
        if not isinstance(self.layers[-1], MenuLayer):
            self.menu.open()
            return EVENT_HANDLED
        return EVENT_UNHANDLED

    def load_music(self, filename):
        self._player.queue(media.load(filename))

    def play_music(self):
        self._player.play()

    def pause_music(self):
        self._player.pause()

    def stop_music(self):
        self._player.pause()
        self._player.next_source()


class TiledGameScene(GameScene):
    GROUND_Z = 16
    SPRITE_Z = 32
    AIR_Z = 64

    def __init__(self, *args, tilemap=None, ground_layers=None,
                 air_layers=None, **kwargs):
        super().__init__(*args, **kwargs)
        self._sprite_layer = ScrollableLayer()
        self.ground_layers = []
        self.air_layers = []
        self.scroller = ScrollingManager(director.window, do_not_scale=True)
        self.scroller.add(self._sprite_layer, z=self.SPRITE_Z)
        self.add_to_game(self.scroller, z=0)
        if tilemap is not None:
            self.load_tilemap(tilemap, ground_layers=ground_layers,
                              air_layers=air_layers)

    def load_tilemap(self, tilemap, ground_layers=None, air_layers=None):
        if ground_layers is None:
            ground_layers = []
        if air_layers is None:
            air_layers = []
        resource = cocos.tiles.load(tilemap)
        for layer_name in ground_layers:
            self.add_tile_layer(z=self.GROUND_Z, collection=self.ground_layers,
                                layer_name=layer_name, resource=resource)
        for layer_name in air_layers:
            self.add_tile_layer(z=self.AIR_Z, collection=self.air_layers,
                                layer_name=layer_name, resource=resource)
        self.scroller.set_focus(0, 0)

    def add_tile_layer(self, layer=None, collection=None,
                       layer_name=None, resource=None, z=0):
        if collection is None:
            collection = self.ground_layers
        if layer is None:
            if layer_name is None:
                raise ValueError('No layer or layer_name given!')
            elif resource is None:
                raise ValueError('No resource given to extract layer from!')
            else:
                layer = resource[layer_name]
        collection.append(layer)
        self.scroller.add(layer, z=z)

    def add_sprite(self, sprite):
        self._sprite_layer.add(sprite)
