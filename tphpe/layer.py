import pyglet.event

from cocos.layer import ColorLayer


class GameLayer(ColorLayer):
    def __init__(self, *args, bg_alpha=255, **kwargs):
        super().__init__(64, 64, 224, bg_alpha, *args, **kwargs)

        self.schedule(self.update)

    def handle_children(self, event_name, args, kwargs):
        result = pyglet.event.EVENT_UNHANDLED
        handlers = self.get_children()
        for h in handlers:
            try:
                result = getattr(h, event_name)(*args, **kwargs)
            except AttributeError:
                continue
            if result == pyglet.event.EVENT_HANDLED:
                break
        return result

    def on_input_press(self, *args, **kwargs):
        return self.handle_children('on_input_press', args, kwargs)

    def on_input_release(self, *args, **kwargs):
        return self.handle_children('on_input_release', args, kwargs)

    def update(self, dt):
        if hasattr(self.parent, 'update'):
            self.parent.update(dt)

    def pause(self):
        super().pause()
        self.pause_scheduler()
        for child in self.get_children():
            child.pause()

    def resume(self):
        super().resume()
        self.resume_scheduler()
        for child in self.get_children():
            child.resume()
