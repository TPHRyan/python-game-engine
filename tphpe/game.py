import cocos.director

from tphpe.base import Root
from tphpe.keyboard import Keyboard


class Game(Root):
    def __init__(self, *args, width=800, height=600,
                 caption='RPG Engine', visible=False, **kwargs):
        super().__init__(*args, **kwargs)
        self.director = cocos.director.director
        game_window = self.director.init(*args, width=width,
                                         height=height, caption=caption,
                                         visible=visible, **kwargs)
        win_x = game_window.screen.width // 2 - game_window.width // 2
        win_y = game_window.screen.height // 2 - game_window.height // 2
        game_window.set_location(win_x, win_y)
        game_window.set_visible(True)

        self.main_input = Keyboard()
        self.director.window.push_handlers(self.main_input)

    def run(self, scene):
        """
        :param tphpe.scene.GameScene scene:
        """
        for controller in scene.controllers:
            self.main_input.push_handlers(controller)
        self.director.run(scene)
