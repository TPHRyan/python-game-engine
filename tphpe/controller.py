from pyglet.event import EVENT_HANDLED
from pyglet.event import EVENT_UNHANDLED

from tphpe.base import Root


class Controller(Root):
    def __init__(self, btns=None, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.button_map = {} if btns is None else btns

    def on_input_press(self, inp, modifiers, call_params=None):
        if call_params is None:
            call_params = tuple()
        if inp in self.button_map:
            try:
                return self.button_map[inp][0](*call_params)
            except TypeError:
                return self.button_map[inp](*call_params)
        return EVENT_UNHANDLED

    def on_input_release(self, inp, modifiers, call_params=None):
        if call_params is None:
            call_params = tuple()
        if inp in self.button_map:
            try:
                if len(self.button_map[inp]) >= 2:
                    return self.button_map[inp][1](*call_params)
            except TypeError:
                pass
        return EVENT_UNHANDLED


class EntityController(Controller):
    def __init__(self, btns=None, *args, ents=None, active_entity=0, **kwargs):
        super().__init__(btns=btns, *args, **kwargs)
        self.active_entity = active_entity
        self.entities = {} if ents is None else ents

    def on_input_press(self, inp, modifiers, **kwargs):
        try:
            call_params = (self.entities[self.active_entity],)
        except KeyError:
            pass
        else:
            return super().on_input_press(inp, modifiers,
                                          call_params=call_params)

    def on_input_release(self, inp, modifiers, **kwargs):
        try:
            call_params = (self.entities[self.active_entity],)
        except KeyError:
            pass
        else:
            return super().on_input_release(inp, modifiers,
                                            call_params=call_params)


class ObjectController(Controller):
    def __init__(self, btns=None, *args, obj=None, **kwargs):
        super().__init__(btns=btns, *args, **kwargs)
        self.object = obj

    def on_input_press(self, inp, modifiers, **kwargs):
        call_params = (self.object,)
        return super().on_input_press(inp, modifiers, call_params=call_params)

    def on_input_release(self, inp, modifiers, **kwargs):
        call_params = (self.object,)
        return super().on_input_release(inp, modifiers, call_params=call_params)
