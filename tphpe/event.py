from pyglet.event import EventDispatcher as PygEventDispatcher


class EventDispatcher(PygEventDispatcher):
    @classmethod
    def register_event_type(cls, name):
        super().register_event_type('on_'+name)

    def dispatch_event(self, event_type, *args):
        super().dispatch_event('on_'+event_type, *args)
