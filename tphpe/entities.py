from itertools import count
from tphpe.base import Root
from tphpe.event import EventDispatcher


class EntityManager(Root, EventDispatcher):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.available_ids = count(start=1)
        self.entities = {}

        self.paused = False

    def update(self, dt):
        if self.paused:
            return

    def pause(self):
        self.paused = True

    def resume(self):
        self.paused = False

    def spawn(self, entity, **kwargs):
        if isinstance(entity, dict):
            ent_id = next(self.available_ids)
            entity['id'] = ent_id
            self.entities[ent_id] = entity
            self.dispatch_event('entity_spawn', entity)
            return entity
        else:
            raise ValueError('EntityManager: Entity spawned'
                             ' must be of type dict')

    def kill(self, to_kill):
        if isinstance(to_kill, dict):
            entity = to_kill
            entity_id = entity['id']
        elif isinstance(to_kill, int):
            entity = self.entities.get(to_kill, None)
            entity_id = to_kill
        else:
            raise ValueError('EntityManager: Must pass in entity'
                             ' or entity ID to be killed')
        if entity_id in self.entities:
            self.entities.pop(entity_id)
            self.dispatch_event('entity_death', entity)

EntityManager.register_event_type('entity_spawn')
EntityManager.register_event_type('entity_death')
