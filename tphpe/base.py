from enum import Enum
from types import MethodType

from cocos.euclid import Vector2


def make_vector2(*args):
    if len(args) > 1:
        return Vector2(x=args[0], y=args[1])
    elif isinstance(args[0], tuple):
        return Vector2(x=args[0][0], y=args[0][1])
    elif isinstance(args[0], Vector2):
        return args[0]
    return Vector2()


class Axis(Enum):
    X = 0
    Y = 1
    BOTH = 2


class Root(object):
    def __init__(self, *args, **kwargs):
        super().__init__()

    def __getattr__(self, item):
        assert not hasattr(super(), item)
        return self.do_nothing

    def do_nothing(self, *args, **kwargs):
        pass
