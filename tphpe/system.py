from abc import ABCMeta
from abc import abstractmethod

from tphpe.base import Root
from tphpe.event import EventDispatcher

# MotionSystem
from cocos.actions.move_actions import Move

# CollisionSystem
from cocos.sprite import Sprite
from cocos.collision_model import AARectShape
from cocos.collision_model import CollisionManagerGrid
from cocos.euclid import Vector2
from tphpe.base import make_vector2


class SystemManager(Root, EventDispatcher):
    available_systems = None

    def __init__(self, *args, game_scene=None, **kwargs):
        super().__init__(*args, **kwargs)
        if SystemManager.available_systems is None:
            SystemManager.available_systems = {}
            for subclass in System.__subclasses__():
                SystemManager.available_systems[subclass.type] = subclass
        self.enabled_systems = {}
        self.disabled_systems = {}
        self.game_scene = game_scene
        self.paused = False

    def update(self, dt, *args, **kwargs):
        for system in self.enabled_systems.values():
            system.update(dt, *args, **kwargs)

    def pause(self):
        self.paused = True

    def resume(self):
        self.paused = False

    def enable_system(self, *system_names):
        for system_name in system_names:
            if system_name in self.enabled_systems:
                pass
            else:
                # If system previously disabled, resume
                # TODO: Change behaviour to 'recalibrate' on re-enable
                if system_name in self.disabled_systems:
                    self.enabled_systems[system_name] = \
                        self.disabled_systems[system_name]
                # If system enabled for first time, create new system
                elif system_name in self.available_systems:
                    self.enabled_systems[system_name] = \
                        self.available_systems[system_name](
                            game_scene=self.game_scene, manager=self)
                # If system doesn't exist, this is a problem.
                else:
                    raise SystemNotFoundError(
                        '{} not found.'.format(system_name))
                self.dispatch_event('system_enable',
                                    self.enabled_systems[system_name])

    def disable_system(self, *system_names):
        for system_name in system_names:
            system = self.enabled_systems.pop(system_name, None)
            if system is not None:
                self.dispatch_event('system_disable',
                                    self.enabled_systems[system_name])

    def on_entity_spawn(self, entity):
        for system in self.enabled_systems.values():
            system.on_entity_spawn(entity)

    def on_entity_death(self, entity):
        for system in self.enabled_systems.values():
            system.on_entity_death(entity)

SystemManager.register_event_type('system_enable')
SystemManager.register_event_type('system_disable')


class SystemNotFoundError(KeyError):
    pass


class System(Root, metaclass=ABCMeta):
    type = 'base_system'

    def __init__(self, *args, game_scene=None, manager=None, **kwargs):
        super().__init__(*args, **kwargs)
        self.manager = manager
        self.game_scene = game_scene
        self.entities = {}

    def add_entity(self, entity):
        self.entities[entity['id']] = entity

    @abstractmethod
    def update(self, dt, *args, **kwargs):
        pass

    def on_entity_spawn(self, entity):
        if self.type in entity:
            self.entities[entity['id']] = entity

    def on_entity_death(self, entity):
        self.entities.pop(entity['id'], None)


class RenderSystem(System):
    type = 'cocosnode'

    def update(self, dt, *args, **kwargs):
        super().update(dt, *args, **kwargs)

    def on_entity_spawn(self, entity):
        super().on_entity_spawn(entity)
        if self.type in entity:
            component = entity[self.type]
            if self.game_scene is not None and 'node' in component:
                self.game_scene.add_to_game(component['node'])

    def on_entity_death(self, entity):
        super().on_entity_death(entity)
        if self.type in entity:
            component = entity[self.type]
            if self.game_scene is not None and 'node' in component:
                self.game_scene.remove_from_game(component['node'])


class MotionSystem(System):
    type = 'motion'

    def update(self, dt, *args, **kwargs):
        super().update(dt, *args, **kwargs)
        for entity in self.entities.values():
            if RenderSystem.type in entity and 'node' in entity[RenderSystem.type]:
                node = entity[RenderSystem.type]['node']
                node.velocity = entity[self.type]['velocity']

    def on_entity_spawn(self, entity):
        super().on_entity_spawn(entity)
        if self.type in entity:
            self.add_entity(entity)
            rs_type = RenderSystem.type
            if rs_type in entity and 'node' in entity[rs_type]:
                entity[rs_type]['node'].do(Move())


class CollisionSystem(System):
    type = 'collision'

    @staticmethod
    def make_bounding_box(sprite: Sprite):
        sprite_rect = sprite.get_rect()
        return AARectShape(make_vector2(sprite_rect.center),
                           sprite_rect.width // 2, sprite_rect.height // 2)

    def __init__(self, *args, xmin=0, xmax=1024, ymin=0, ymax=768,
                 cell_width=64, cell_height=64, **kwargs):
        super().__init__(*args, **kwargs)
        self.collision_manager = CollisionManagerGrid(
                xmin, xmax, ymin, ymax, cell_width, cell_height)

    def update(self, dt, *args, **kwargs):
        super().update(dt, *args, **kwargs)
        self.collision_manager.clear()
        rs_type = RenderSystem.type
        for entity in self.entities.values():
            if rs_type in entity and 'node' in entity[rs_type]:
                entity[self.type]['cshape'].center = Vector2(
                        entity[rs_type]['node'].x, entity[rs_type]['node'].y)
                self.collision_manager.add(entity[rs_type]['node'])

        for entity in self.entities.values():
            if rs_type in entity and 'node' in entity[rs_type]:
                objs_colliding = self.collision_manager.objs_colliding(
                        entity[rs_type]['node'])
                for coll_obj in objs_colliding:
                    # Something is colliding, probably do an event
                    pass

    def on_entity_spawn(self, entity):
        super().on_entity_spawn(entity)
        if self.type in entity:
            self.add_entity(entity)
            rs_type = RenderSystem.type
            if (rs_type in entity and 'node' in entity[rs_type] and
                    isinstance(entity[rs_type]['node'], Sprite)):
                spr = entity[rs_type]['node']
                if not ('cshape' in entity[self.type]):
                    entity[self.type]['cshape'] = self.make_bounding_box(spr)
                spr.cshape = entity[self.type]['cshape']
                self.collision_manager.add(spr)
