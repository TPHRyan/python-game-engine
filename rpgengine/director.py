from cocos.director import director


def init(*args, width=800, height=600,
         caption='RPG Engine', visible=False, **kwargs):
    game_window = director.init(*args, width=width,
                                height=height, caption=caption,
                                visible=visible, **kwargs)
    win_x = game_window.screen.width // 2 - game_window.width // 2
    win_y = game_window.screen.height // 2 - game_window.height // 2
    game_window.set_location(win_x, win_y)
    game_window.set_visible(True)
    return game_window
