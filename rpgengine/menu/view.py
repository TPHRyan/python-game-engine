from cocos.layer import ColorLayer
from cocos.layer import MultiplexLayer
from cocos.text import Label
from pyglet.event import EVENT_UNHANDLED
from rpgengine.menu.menu import MenuData


class ListView(ColorLayer):
    """
    Represents a menu in the form of a list.
    """
    def __init__(self, *args, **kwargs):
        super().__init__(0, 0, 0, 128, **kwargs)

    def build(self, menu_data):
        placeholder_label = Label('TEXT GOES HERE', position=(45, 45))
        self.add(placeholder_label)
        return self

    def select(self, ind):
        return self


class MenuLayer(MultiplexLayer):
    """
    Represents a set of menus as they will be displayed in a big-picture form
    """
    def __init__(self, *args, view_type=ListView, key_handler=None, **kwargs):
        # Create a view for this menu in the constructor
        super().__init__(view_type(), *args)
        self.menu_index = {}
        self.view_type = view_type
        self.key_handler = key_handler

    def build(self, menu_data):
        # Get a clean slate
        for i, c in self.children[1:]:
            self.remove(c)
        # Build the layer we created in the constructor
        self.children[0][1].build(menu_data)

        # Create layers for any submenus and build them
        for i, menu_item in enumerate(menu_data):
            if isinstance(menu_item.action, MenuData):
                self.menu_index[i] = len(self.children)
                self.add(
                    MenuLayer(view_type=self.view_type).build(menu_item.action)
                )
        return self

    def sync(self, pos_vector):
        # If we have 1 position left then it's the selected item in the menu
        if len(pos_vector) == 1:
            self.switch_to(0)
            self.children[0][1].select(pos_vector[0])
        # Otherwise we switch to the correct submenu and let it deal with
        # the rest.
        else:
            child_index = self.menu_index[pos_vector[0]]
            self.switch_to(child_index)
            self.children[0][1].select(pos_vector[0])
            self.children[child_index][1].sync(pos_vector[1:])
        return self

    def set_key_handler(self, kh):
        self.key_handler = kh

    def on_key_press(self, key, modifiers):
        if self.key_handler is not None:
            return self.key_handler.on_key_press(key, modifiers)
        return EVENT_UNHANDLED

    def on_key_release(self, key, modifiers):
        if self.key_handler is not None:
            return self.key_handler.on_key_release(key, modifiers)
        return EVENT_UNHANDLED
