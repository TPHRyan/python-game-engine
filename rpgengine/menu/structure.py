from collections import namedtuple
from rpgengine.base import Root
from rpgengine.menu.exception import MenuException

_MenuItem = namedtuple('_MenuItem', ('text', 'action'))


class MenuCommand(Root):
    """
    Simple class for encapsulation of a menu item's effect
    """
    def __init__(self, callback, *args, uses_menu=False, **kwargs):
        super().__init__(*args, **kwargs)
        self.callback = callback
        self.args = args
        self.kwargs = kwargs
        self.uses_menu = uses_menu

    def __call__(self, menu, *args, **kwargs):
        if self.uses_menu:
            return self.callback(menu, *self.args, *args,
                                 **self.kwargs, **kwargs)
        else:
            return self.callback(*self.args, *args, **self.kwargs, **kwargs)


class MenuData(Root):
    """
    Object that represents the linear structure of a menu
    """
    def __init__(self, *args, default_position=0, **kwargs):
        super().__init__(*args, **kwargs)
        self._data = tuple(args)
        self.default_position = default_position
        self.validate_items()

    def validate_items(self):
        for item in self:
            if not (isinstance(item, MenuItem)):
                raise MenuException('Menu contains item which is not of type '
                                    '{}'.format(MenuItem.__name__))

    def __repr__(self):
        return 'MenuData{}'.format(super().__repr__())

    def __getitem__(self, item):
        return self._data[item]

    def __iter__(self):
        yield from self._data

    def __len__(self):
        return len(self._data)


class MenuItem(_MenuItem):
    """
    An item in a menu, consisting of text and an action (can be a submenu)
    """
    def __init__(self, *args, **kwargs):
        super().__init__()

    def validate(self):
        if not isinstance(self.text, str):
            raise MenuException('{} created with invalid '
                                'text label'.format(self.__class__.__name__))

        if not (
                    isinstance(self.action, MenuCommand) or
                    isinstance(self.action, MenuData)
        ):
            raise MenuException('{} created with invalid '
                                'action'.format(self.__class__.__name__))
