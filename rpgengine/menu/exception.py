class MenuException(ValueError):
    """
    An exception for use whenever an invalid menu is initialized
    """
    pass

