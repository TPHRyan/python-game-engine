from rpgengine.base import Root


class EntityManager(Root):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.entities = []
        self.systems = {}

    def update(self, dt):
        for sys_type, system in self.systems.items():
            system.update(dt)

    def tick(self):
        for sys_type, system in self.systems.items():
            system.tick()
