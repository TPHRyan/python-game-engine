from collections import defaultdict
from pyglet.event import EventDispatcher


class Keyboard(defaultdict, EventDispatcher):
    def __init__(self, **kwargs):
        super().__init__(lambda: False, **kwargs)

    def press(self, key, modifiers):
        self[key] = True
        self.dispatch_event('on_key_press', key, modifiers)

    def release(self, key, modifiers):
        self[key] = False
        self.dispatch_event('on_key_release', key, modifiers)

Keyboard.register_event_type('on_key_press')
Keyboard.register_event_type('on_key_release')
