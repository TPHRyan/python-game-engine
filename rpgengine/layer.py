import pyglet.event

from cocos.layer import ColorLayer
from rpgengine.entity import EntityManager


class GameLayer(ColorLayer):
    is_event_handler = True
    TPS = 30

    def __init__(self, *args, bg_alpha=255, **kwargs):
        super().__init__(64, 64, 224, bg_alpha, *args, **kwargs)
        self.entity_manager = EntityManager()

        self.schedule(self.update)
        self.schedule_interval(self.tick, 1/self.TPS)

    def handle_children(self, event_name, args, kwargs):
        result = pyglet.event.EVENT_UNHANDLED
        _, children = zip(*self.children)
        handlers = list(children)
        handlers.append(self.parent)
        for h in handlers:
            try:
                result = getattr(h, event_name)(*args, **kwargs)
            except AttributeError:
                continue
            if result == pyglet.event.EVENT_HANDLED:
                break
        return result

    def on_key_press(self, *args, **kwargs):
        return self.handle_children('on_key_press', args, kwargs)

    def on_key_release(self, *args, **kwargs):
        return self.handle_children('on_key_release', args, kwargs)

    def update(self, dt):
        if hasattr(self.parent, 'update'):
            self.parent.update(dt)
        self.entity_manager.update(dt)

    def tick(self, dt):
        if hasattr(self.parent, 'tick'):
            self.parent.tick()
        self.entity_manager.tick()

    def pause(self):
        super().pause()
        self.pause_scheduler()
        for i, child in self.children:
            child.pause()

    def resume(self):
        super().resume()
        self.resume_scheduler()
        for i, child in self.children:
            child.resume()
