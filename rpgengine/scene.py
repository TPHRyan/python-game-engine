import cocos.tiles
import pyglet.event
import pyglet.media as media
import pyglet.window.key as pyglet_key

from cocos.layer.scrolling import ScrollableLayer
from cocos.layer.scrolling import ScrollingManager
from cocos.scene import Scene
from pyglet.event import EventDispatcher
from rpgengine.director import director
from rpgengine.keyboard import Keyboard
from rpgengine.layer import GameLayer
from rpgengine.menu import ListView
from rpgengine.menu import Menu
from rpgengine.menu import MenuData


class GameScene(Scene, EventDispatcher):
    def __init__(self, *args, internal_layer=GameLayer,
                 menu_data=MenuData(), menu_view_type=ListView, **kwargs):
        super().__init__(*args)
        self._game_layer = internal_layer()
        self.add(self._game_layer)

        self.keyboard = Keyboard()
        self._player = media.Player()
        self.music = None
        self.menu = Menu(self, menu_data=menu_data, menu_view=menu_view_type)

    def update(self, dt):
        pass

    def tick(self):
        pass

    def pause_game(self):
        self.pause_music()
        self._game_layer.pause()

    def resume_game(self):
        self.play_music()
        self._game_layer.resume()

    def add_to_game(self, *args, **kwargs):
        self._game_layer.add(*args, **kwargs)

    def on_key_press(self, key, modifiers):
        self.keyboard.press(key, modifiers)
        if key == pyglet_key.ESCAPE:
            self.open_menu()
            return pyglet.event.EVENT_HANDLED
        return pyglet.event.EVENT_UNHANDLED

    def on_key_release(self, key, modifiers):
        self.keyboard.release(key, modifiers)
        return pyglet.event.EVENT_UNHANDLED

    def open_menu(self):
        self.menu.open()

    def load_music(self, filename):
        self._player.queue(media.load(filename))

    def play_music(self):
        self._player.play()

    def pause_music(self):
        self._player.pause()

    def stop_music(self):
        self._player.pause()
        self._player.next_source()


class TiledGameScene(GameScene):
    GROUND_Z = 16
    SPRITE_Z = 32
    AIR_Z = 64

    def __init__(self, *args, tilemap=None, ground_layers=None,
                 air_layers=None, **kwargs):
        super().__init__(*args, **kwargs)
        self._sprite_layer = ScrollableLayer()
        self.ground_layers = []
        self.air_layers = []
        self.scroller = ScrollingManager(director.window, do_not_scale=True)
        self.scroller.add(self._sprite_layer, z=self.SPRITE_Z)
        self.add_to_game(self.scroller, z=0)
        if tilemap is not None:
            self.load_tilemap(tilemap, ground_layers=ground_layers,
                              air_layers=air_layers)

    def load_tilemap(self, tilemap, ground_layers=None, air_layers=None):
        if ground_layers is None:
            ground_layers = []
        if air_layers is None:
            air_layers = []
        resource = cocos.tiles.load(tilemap)
        for layer_name in ground_layers:
            self.add_tile_layer(z=self.GROUND_Z, collection=self.ground_layers,
                                layer_name=layer_name, resource=resource)
        for layer_name in air_layers:
            self.add_tile_layer(z=self.AIR_Z, collection=self.air_layers,
                                layer_name=layer_name, resource=resource)
        self.scroller.set_focus(0, 0)

    def add_tile_layer(self, layer=None, collection=None,
                       layer_name=None, resource=None, z=0):
        if collection is None:
            collection = self.ground_layers
        if layer is None:
            if layer_name is None:
                raise ValueError('No layer or layer_name given!')
            elif resource is None:
                raise ValueError('No resource given to extract layer from!')
            else:
                layer = resource[layer_name]
        collection.append(layer)
        self.scroller.add(layer, z=z)

    def add_sprite(self, sprite):
        self._sprite_layer.add(sprite)
